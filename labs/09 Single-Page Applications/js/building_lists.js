
var request = new XMLHttpRequest();
request.open('GET', 'data/books.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var books = data.books;

let header = document.createElement('h1');
document.body.appendChild(header);

let list = document.createElement('table');

let listHeader1 = document.createElement('th');
listHeader1.innerHTML = "Name";
list.appendChild(listHeader1);
let listHeader2 = document.createElement('th');
listHeader2.innerHTML = "Year";
list.appendChild(listHeader2);


for (let i=0; i < books.length; i++) {
	console.log(books[i].title);

	let tr = document.createElement('tr');
	list.appendChild(tr);

	tr.onclick = function() {
		header.innerHTML = books[i].title;
	}

	let td = document.createElement('td');
	td.innerHTML = books[i].title;
	tr.appendChild(td);

	let td2 = document.createElement('td');
	td2.innerHTML = books[i].year;
	tr.appendChild(td2);
}

document.body.appendChild(list);
