(function (window) {
    function $() {

    }

    $.prototype.id = function(id) {
        return document.getElementById(id);
    }

    window.app = window.app || {};
    window.app.$ = $;
}) (window);